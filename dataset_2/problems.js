let data = [{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp","gender":"Male","ip_address":"253.171.63.171"},
{"id":2,"first_name":"Kenneth","last_name":"Hinemoor","email":"khinemoor1@yellowbook.com","gender":"Polygender","ip_address":"50.231.58.150"},
{"id":3,"first_name":"Roman","last_name":"Sedcole","email":"rsedcole2@addtoany.com","gender":"Genderqueer","ip_address":"236.52.184.83"},
{"id":4,"first_name":"Lind","last_name":"Ladyman","email":"lladyman3@wordpress.org","gender":"Male","ip_address":"118.12.213.144"},
{"id":5,"first_name":"Jocelyne","last_name":"Casse","email":"jcasse4@ehow.com","gender":"Agender","ip_address":"176.202.254.113"},
{"id":6,"first_name":"Stafford","last_name":"Dandy","email":"sdandy5@exblog.jp","gender":"Female","ip_address":"111.139.161.143"},
{"id":7,"first_name":"Jeramey","last_name":"Sweetsur","email":"jsweetsur6@youtube.com","gender":"Genderqueer","ip_address":"196.247.246.106"},
{"id":8,"first_name":"Anna-diane","last_name":"Wingar","email":"awingar7@auda.org.au","gender":"Agender","ip_address":"148.229.65.98"},
{"id":9,"first_name":"Cherianne","last_name":"Rantoul","email":"crantoul8@craigslist.org","gender":"Genderfluid","ip_address":"141.40.134.234"},
{"id":10,"first_name":"Nico","last_name":"Dunstall","email":"ndunstall9@technorati.com","gender":"Female","ip_address":"37.12.213.144"}];


// 1. Find all people who are Agender

const agenderPeople = data.filter((item) => {
    return item['gender'] === 'Agender';
});

// console.log(agenderPeople);


// 2. Split their IP address into their components eg. 111.139.161.143 has components 111 139 161 143.

const updatedata = data.map((object) => {
    let newObject = { ...object };
    newObject['new_ip_address'] = newObject['ip_address'].split('.').join(' ');
    return newObject;
});

// console.log(updatedata);


// 3. Find the sum of all the second components of the ip addresses.

const sumOf2ndCompponents = updatedata.reduce((val, object) => {
     object = Number(object['new_ip_address'].split(' ')[1]);
     return val + object;
},0);

// console.log(sumOf2ndCompponents);


// 4. Find the sum of all the fourth components of the ip addresses.

const sumOf4thCompponents = updatedata.reduce((val, object) => {
    object = Number(object['new_ip_address'].split(' ')[3]);
    return val + object;
},0);

// console.log(sumOf4thCompponents);


// 5. Compute the full name of each person

const fullNames = updatedata.map((object) => {
    let newObject = { ...object };
    newObject['full_name'] = `${newObject['first_name']} ${newObject['last_name']}`;
    return newObject;
});

// console.log(fullNames);


// 6. Filter out all the .org emails

const allOrgMails = fullNames.filter((object) => {
    if(object['email'].indexOf('.org') !== -1){
        return object;
    }
});

// console.log(allOrgMails);


// 7. Calculate how many .org, .au, .com emails are there

const numberOfmailsWithWithOrgAuCom = fullNames.filter((object) => {
    if(object['email'].indexOf('.com') !== -1 || object['email'].indexOf('.au') !== -1 || object['email'].indexOf('.org') !== -1 ){
        return object;
    };
}).length;

// console.log(numberOfmailsWithWithOrgAuCom);


// 8. Sort the data in descending order of first name

const sortedData = fullNames.sort((objectA,objectB) => {
    let nameA = objectA['first_name'];
    let nameB = objectB['first_name'];
    if (nameB < nameA) {
      return -1;
    }
    if (nameB > nameA) {
      return 1;
    }
    return 0;
  });

console.log(sortedData);